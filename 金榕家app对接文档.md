[TOC]

# 基础设置
* webview默认隐藏导航栏
* 允许webview内长按图片保存
* 关闭webview左滑返回功能、关闭下拉回弹功能
* 关闭手指双击放大功能
* 打开webview的时候，缓存需要清空
* webview位置为在”时间栏“下方开始，撑满全屏，如果需要显示原生导航栏，则在导航栏下方开始撑满全屏
* webview的进度条需要显示，不用隐藏


* 设置webview的user-agent,在后面增加app相关信息,格式为appVersion(APP包名称,APP包当前版本号,设备唯一ID)

```
原始数据如
"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
```

```
增加app相关信息后
"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 appVersion(jrj,1.0.0,3304994858592992)"
```

# 原生必带功能

* 消息推送
* 版本强制更新


# 功能索引

* [打开一个URL](#anchor1)
* [分享(微信、QQ、微博等、图片分享)](#anchor4)
* [直接唤起短信，给指定手机号发送短信(待开发)](#anchor5)
* [唤起拨打电话功能(待开发)](#anchor11)
* [保存图片(待开发)](#anchor12)
* [关闭当前webview](#anchor2)

* 【暂不考虑】[获取银行卡号](#anchor3)
* 【暂不考虑】[通知APP用户登录成功](#anchor7)
* 【暂不考虑】[通知APP用户登录成功](#anchor8)
* 【暂不考虑】[打开/关闭原生导航栏](#anchor9)
* 【暂不考虑】[获取用户登录信息](#anchor10)
* 【暂不考虑】[在手机桌面创建H5快捷链接(待开发)](#anchor6)

# 金榕家H5 链接参数约定 + JSBridge 对接规范 

## url参数使用限制

任何一个从App端发起的H5链接，App都会自动补充如下参数：

* terminal：渠道划分（iOS 1001; Android 1002)
（规则：渠道+系统（iOS 001; Android 002)，比如主包Id为1，系统为iOS，则为1001）

* appVersion: App版本

* jwt:登录标识

举例：  
`http://192.168.5.27/finance-h5/msite/?terminal=1001&appVersion=1.0&jwt=xxx#/`


## jsbridge

#### Handler约定
* App端消息监听  
  说明：H5发消息，需要App注册监听  
  handler : "BanianAppHandler"

* H5端消息监听Handler  
  说明：App发消息，需要H5注册监听  
  handler : "BanianH5Handler"  

```
H5端初始化及消息调用代码：

初始化
if (window.WebViewJavascriptBridge) {
  return callback(WebViewJavascriptBridge);
}
if (window.WVJBCallbacks) {
  return window.WVJBCallbacks.push(callback);
}
window.WVJBCallbacks = [callback];
var WVJBIframe = document.createElement('iframe');
WVJBIframe.style.display = 'none';
WVJBIframe.src = 'https://__bridge_loaded__';
document.documentElement.appendChild(WVJBIframe);
setTimeout(function () {
  document.documentElement.removeChild(WVJBIframe)
}, 0)

消息调用，data为json对象
bridge.callHandler('BanianAppHandler', data, callback)

```


# jsbridge功能目录


<span id = "anchor1"></span>
### 打开一个URL

~~~
{
    “priType” : “pageRouter”,
    "secType" : "htmlPage",
    "data" : {
      "linkStr" : "https://www.baidu.com" //默认使用隐藏导航栏+新窗口打开
      //扩展参数
      "safari":false,//默认false,是否使用safari(安卓为外部浏览器)来打开URL
      "nav":false,//默认false,是否展示原生导航栏
    }
}
~~~



<span id = "anchor4"></span>
### 分享(微信、QQ、微博、图片分享等)

~~~
{
    “priType” : “share”,
    "secType" : "default",
    "data" : {
         "type":"分享方式",//link:分享链接,image:分享图片和文案,
         "target":"分享的目标",//wxtimeline:微信朋友圈,wxfriend:微信好友
         //（当分享方式为link时用）
         "title" : "[开心拿诺币，好物随心兑]",//分享框标题
         "subTitle" : "[每天都来逛逛，总有特别的惊喜，心仪好物等着你]",//分享文案
         "iconUrl" : "http://.....jpg",//分享icon
         "linkUrl" : "https://www.baidu.com",//分享完整的链接
         //图片（当分享方式为image时用）
         "image": "base64编码的图片",//图片base64编码,
         "imageTitle":"文案",//配合图片的文案
    }
}
返回:
{
    "result" : "1",// 分享结果 （1 成功；2 取消分享  注：用户选择留在被分享的App则无法获取分享结果）
}
~~~

<span id = "anchor5"></span>
### 直接唤起短信，给指定手机号发送短信(待开发)

~~~
{
    “priType” : "sms",
    "data" : {
      "mobileNum":"1370000002",//发送目标的手机号
      "text":"短信文本体"
    }
}
返回:
{
    "result" : "1",//发送结果，1：成功，0：取消，-1：其他原因发送失败
}
~~~


<span id = "anchor11"></span>
### 唤起拨打电话功能(待开发)

~~~
{
    “priType” : "tel",
    "data" : {
      "mobileNum":"1370000002",//目标的手机号
    }
}
返回:
{
    "result" : "1",//发送结果，1：成功，0：取消，-1：其他原因发送失败
}
~~~



<span id = "anchor12"></span>
### 保存图片(待开发)

~~~
{
    “priType” : "saveImage",
    "data" : {
      "img":"base64图片编码的字符串",
    }
}
返回:
{
    "result" : "1",//发送结果，1：成功，0：取消，-1：其他原因发送失败
}
~~~



<span id = "anchor2"></span>
###  关闭当前webview

~~~
{
    “priType” : “closePage”,//如果当前webview是唯一的webview则不需要做任何处理
    "data" : {
      "force":false,//默认是false，为true时强行关闭窗口
    }
}
~~~

<span id = "anchor3"></span>
### 获取银行卡号

~~~
{
    “priType” : “getInfo”,
    "secType" : "bankCardNumber",
}
返回：
{
    "bankCardNumber" : "xxxxxx",//获取的银行卡号
}
~~~

<span id = "anchor6"></span>
### 在手机桌面创建H5快捷链接(待开发)

~~~
{
    “priType” : "createWebApp",
    "data" : {
      "link":"https://www.baidu.com",//完整链接
    }
}
返回:
{
    "result" : "1",//发送结果，1：成功，0：取消，-1：其他原因发送失败
}
~~~

<span id = "anchor7"></span>
### 通知APP用户登录成功

~~~
{
    “priType” : “infoSyn”,
    "secType" : "login",
    "data" : {
         “jwt” : "xxxxxx"
    }
}
~~~

<span id = "anchor8"></span>
### 通知APP用户退出登录

~~~
{
    “priType” : “infoSyn”,
    "secType" : "logout",
}
~~~

<span id = "anchor9"></span>
### 打开/关闭原生导航栏

~~~
{
    “priType” : “showNav”,
    "secType" : "show",//show :展示；hide 隐藏
    "data" : {
    }
}
~~~


<span id = "anchor10"></span>
### 获取用户登录信息

~~~
{
    “priType” : “getInfo”,
    "secType" : "loginSign",
    "data" : {
         "channel" : "wechat",//登录渠道，枚举 "wechat" 、"qq"
    }
}
返回：
{
    "jwt" : "xxxxxx",
    "isBind" : true,
    "openId" : ""
}
~~~


# 二期功能

* 消息推送的细节，需要能进行点对点推送

安卓
* 拨打电话的时候需要弹出对话框提示是否拨打号码，用户点击确认再拨打


加载优化方案
* 增加加载loading页，用于当webview在加载中的时候，展示
* 启动页-启动页打开时自动后台加载h5页面
* webview实现缓存模式
  1、后端提供接口查询H5的版本号
  2、根据版本号和本地存储的版本号对比进行清空webview缓存的动作
* 当webview加载Url异常时，如404等故障等，弹出提示框告知网络故障，可以点击重试按钮重新加载，如果当前webview不是最后一个窗口，则有取消按钮，点击后关闭当前webview窗口







